import React, { Component } from 'react'
import { glassArr } from './DataItem'
import GlassItem from './GlassItem'
import './background.css'
export default class RenderGlass extends Component {
    state = {
        glassArr: glassArr,
        imgSrc: "./glassesImage/model.jpg",
        imgShow: "",
        id: "",
        description:"",
        status: "none",
    }
    HandleShowGlass = (item) =>{
        this.setState({
            imgShow: `./glassesImage/v${item.id}.png`,
            name: item.name,
            description: item.desc,
            status: "block",
        })
    }
  render() {
    return (
      <div className='back_ground'>
      <div className='container py-5'>
        <div className='row d-flex justify-content-center'>
          <div style={{position:"relative"}} className="col-3">
                <img style={{width:"15rem"}} className="FirstIma" src={this.state.imgSrc} alt=""></img>
                <img style={{width:"8rem",position:"absolute",top:"73px",left:"72px",opacity:"0.8"}} 
                src={this.state.imgShow}></img>
                <div 
                style={{color:"white",width:"100%",position:"absolute",bottom:"0",left:"0",backgroundColor:"rgba(0,0,0,0.7)",display:`${this.state.status}`}}
                className='detailedText'>
                    <h2 style={{fontSize:"16px"}}>{this.state.name}</h2>
                    <p style={{fontSize:"14px"}}>{this.state.description}</p>
                </div>
          </div>
          <div className='col-3'>
                <img style={{width:"15rem"}} className="SecondIma"
                src={this.state.imgSrc} alt=""></img>
          </div>
        </div>
        <div  className='d-flex justify-content-center'>
            <GlassItem 
                Data={glassArr}
                HandleShowGlass={this.HandleShowGlass}
            ></GlassItem>
          </div>
      </div>
      </div>
    )
  }
}
